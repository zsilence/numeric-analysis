from mpl_toolkits.mplot3d import axes3d
from matplotlib import pyplot
from sympy.abc import x, y

import sympy as sp
import numpy as np
import math as mt

import logging
import sys

logger = logging.getLogger("logger")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))

A = 0.180
B = 0.065
R = 0.025
H = 0.002
P = 7e10
E = 40
nu = 0.3
phi = 0.


def nod_in(x, y):
    return (x - A/2)**2 + (y - B)**2 > R**2

def sb(i, j):
    return sp.Symbol("c%02d%02d" %(i, j))

def main():
    f = E*H**3/(12*(1-nu**2))

    N = int(raw_input("enter number of x steps: "))
    h = A/N
    M = int(B/h)

    c = set([sb(1, 1)])

    system = []

    for j in xrange(N + 1):
        c.add(sb(0, j))
        system.append(sb(0, j) - phi)

    r1, r2 = 0., 0.

    for i in xrange(1, M):
        c.add(sb(i, 0))
        system.append(sb(i, 0) - phi)

        for j in xrange(1, N):
            if nod_in((j+1)*h, i*h) and nod_in((j-1)*h, i*h) and nod_in(j*h, (i+1)*h):
                c.add(sb(i + 1, j))
                c.add(sb(i, j + 1))
                system.append(-4*sb(i, j) + sb(i + 1, j) + sb(i - 1, j) + sb(i, j + 1) + sb(i, j - 1) - h**2*f)
            elif nod_in(j*h, i*h):
                if not nod_in(j*h, (i + 1)*h) and not nod_in(j*h, B):
                    c.add(sb(i, j))
                    r1 = h
                    r2 = min(sp.solve((j*h - A/2)**2 + (y - B)**2 - R**2, y)) - i*h
                    system.append(-sb(i, j) + r1/(r1 + r2)*sb(i - 1, j) + r2/(r1 + r2)*phi)
                # elif not nod_in((j + 1)*h, i*h):
                #     c.add(sb(i, j))
                #     r1 = h
                #     r2 = min(sp.solve((x - A/2)**2 + (i*h - B)**2 - R**2, x)) - j*h
                #     system.append(-sb(i, j) + r1/(r1 + r2)*sb(i, j - 1) + r2/(r1 + r2)*phi)
                # elif not nod_in((j - 1)*h, i*h):
                #     c.add(sb(i, j))
                #     r1 = h
                #     r2 = j*h - max(sp.solve((x - A/2)**2 + (i*h - B)**2 - R**2, x))
                #     system.append(-sb(i, j) + r1/(r1 + r2)*sb(i, j + 1) + r2/(r1 + r2)*phi)
                else:
                    system.append(sb(i, j) - phi)
            else:
                pass

        c.add(sb(i, N))
        system.append(sb(i, N) - phi)

    j = 0
    while nod_in(j*h, M*h):
        c.add(sb(M, j))
        c.add(sb(M, N - j))
        system.append(sb(M, j) - phi)
        system.append(sb(M, N - j) - phi)
        j = j + 1

    c = list(c)
    c.sort(key=lambda x: str(x))
    u = list(sp.linsolve(system, *c))[0]

    logger.info("result: ")
    for k in xrange(len(u)):
        logger.info("{0} = {1}".format(c[k], u[k]))
    logger.info("\nprecision ~ {}".format(h**2))

    xv = [i*h for i in xrange(N + 1)]
    yv = [i*h for i in xrange(M + 1)]

    X, Y = np.meshgrid(xv, yv)
    Z = np.zeros((M + 1, N + 1))

    for k in xrange(len(c)):
        j = int(str(c[k])[3:5])
        i = int(str(c[k])[1:3])
        Z[i, j] = u[k]

    fig = pyplot.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.plot_wireframe(X, Y, Z)
    # ax.plot_surface(X, Y, Z)
    pyplot.show()


if __name__ == "__main__":
    main()
