from sympy.tensor.array import MutableDenseNDimArray as array

from matplotlib import pyplot
from sympy.abc import x, t

import sympy as sp
import numpy as np

import math
import logging
import sys

logger = logging.getLogger("logger")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))

T = 1.
L = 0.1
du = 0.001
E = 1.1e11
r = 4.3e3
phi = 0.

a = r/E


def rho(v):
    if v > L/2:
        return 2*du*(1 - v/L)
    else:
        return 2*du*v/L

def q(v):
    return -du*math.sin(2*math.pi*v/L)

def main():
    N = int(raw_input("enter number of x steps: "))
    M = int(math.ceil(T*N/L))

    h = L/N
    tau = T/M

    xv = [a + i*h for i in xrange(N+1)]
    tv = [a + i*tau for i in xrange(M+1)]

    u = array([0.]*(M + 1)*(N + 1), (M + 1, N + 1))

    for i in xrange(M + 1):
        u[i, 0] = phi
        u[i, N] = phi
    for j in xrange(1, N):
        u[0, j] = rho(xv[j])
        u[1, j] = rho(xv[j]) + tau*q(xv[j])

    for i in xrange(2, M + 1):
        for j in xrange(1, N):
            u[i, j] = a*tau**2/h**2*(u[i-1, j+1] - 2*u[i-1, j] + u[i-1, j-1]) + 2*u[i-1, j] - u[i-2, j]

    u = np.array(u).reshape((M + 1, N + 1))

    logger.info("\nresult:")
    for line in u:
        logger.info(", ".join(["%.4f" % i for i in line]))
    logger.info("\nprecision ~ {}".format(h**2 + tau**2))

    for i in xrange(0, M + 1, 5):
        pyplot.plot(xv, u[i, :])
    pyplot.show()


if __name__ == "__main__":
    main()
