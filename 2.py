from matplotlib import pyplot

import sympy as sp

import math
import logging
import sys

logger = logging.getLogger("logger")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


def diff(yk1, yk, yk_1, x, h, task):
    if task == 1:
        return math.sin(1)*(yk1 - 2*yk + yk_1)/(h**2) + (1 + math.cos(1)*x**2)*yk + 1
    elif task == 2:
        return (yk1 - 2*yk + yk_1)/(h**2) + (0.5 + math.sin(x)**2)*(yk1 - yk_1)/(2*h) + 2*(1 + x**2)*yk - 10*(1 + math.sin(x)**2)
    else:
        return (yk1 - 2*yk + yk_1)/(h**2) - x*(yk1 - yk_1)/(2*h) + 0.2*yk - x - 1

def calculate(n, h, task, a, A, B):
    x = [a + h*i for i in xrange(n + 1)]
    y = [0.0] * (n + 1)
    sy = [sp.Symbol("y{}".format(i)) for i in xrange(n + 1)]

    if task == 3:
        system = [
            sy[0] - 0.5*(-sy[2] + 4*sy[1] - 3*sy[0])/(2*h) - A,
            sy[n] - B
        ]
    else:
        system = [sy[0] - A, sy[n] - B]

    for i in xrange(n - 1):
        system.append(diff(sy[i + 2], sy[i + 1], sy[i], x[i + 1], h, task))

    result = list(sp.linsolve(system, *sy))[0]
    for i in xrange(n + 1):
        y[i] = result[i]

    for i in xrange(n + 1):
        logger.info("{} = 0".format(system[i]))
    logger.info("\n{}\n".format([(x[i], y[i]) for i in xrange(n + 1)]))

    return x, y

def main():
    task = int(raw_input("enter task number: "))
    if task == 1:
        precision = 0.001
        a, b = -1.0, 1.0
        A, B = 0.0, 0.0
    elif task == 2:
        precision = 0.02
        a, b = 0.0, 2.0
        A, B = 0.0, 4.0
    elif task == 3:
        precision = 0.04
        a, b = 0.9, 2.9
        A, B = 2.0, 1.0
    else:
        sys.exit(1)

    itr = 1
    n = 2
    h = (b - a)/n
    e = 1.0
    logger.info("## step {} ##".format(itr))
    x, y = calculate(n, h, task, a, A, B)

    while e > precision:
        itr = itr + 1
        n = n*2
        h = (b - a)/n
        logger.info("## step {} ##".format(itr))
        x0, y0 = calculate(n, h, task, a, A, B)
        e = max([abs((y0[i*2] - y[i])/(2**2 - 1)) for i in xrange(n/2)])
        x, y = x0, y0

    logger.info("iterations: {0}, step: {1}, points: {2}".format(itr, h, n))
    logger.info("precision: {}".format(e))

    pyplot.plot(x, y)
    pyplot.show()


if __name__ == "__main__":
    main()
