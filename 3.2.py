from sympy.tensor.array import MutableDenseNDimArray as array

from matplotlib import pyplot
from sympy.abc import x, t

import sympy as sp
import numpy as np

import math
import logging
import sys

logger = logging.getLogger("logger")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


def main():
    a = 0.0
    b = 1.0
    T = 0.05
    k = 1.0
    phi = 0*x
    g1 = 0*t
    g2 = 0*t
    f = x + 0*t

    N = int(raw_input("enter number of x steps: "))
    M = int(math.ceil(2*T*k*N**2/(b - a)**2))

    h = (b - a)/N
    tau = T/M

    xv = [a + i*h for i in xrange(N+1)]
    tv = [a + i*tau for i in xrange(M+1)]

    u = array([0.]*(M + 1)*(N + 1), (M + 1, N + 1))

    for i in xrange(M + 1):
        u[i, 0] = g1.subs(t, tv[i])
        u[i, -1] = g2.subs(t, tv[i])
    for i in xrange(1, N):
        u[0, i] = phi.subs(x, xv[i])

    for i in xrange(1, M + 1):
        for j in xrange(1, N):
            u[i, j] = u[i-1, j] + k*tau*(u[i-1, j-1] - 2*u[i-1, j] + u[i-1, j+1])/h**2 + tau*f.subs(t, tv[i-1]).subs(x, xv[j])

    u = np.array(u).reshape((M + 1, N + 1))

    logger.info("\nresult of nonstationary task:")
    for line in u:
        logger.info(", ".join(["%.4f" % i for i in line]))
    logger.info("\nprecision ~ {}".format(h**2 + tau))

    # for i in xrange(0, M + 1, 2):
    for i in xrange(0, M + 1):
        pyplot.plot(xv, u[i, :], label="t = {}".format(i*tau))
    pyplot.legend()
    pyplot.show()


if __name__ == "__main__":
    main()
