from sympy.tensor.array import MutableDenseNDimArray as array

from matplotlib import pyplot
from sympy.abc import x, t

import sympy as sp
import numpy as np

import math
import logging
import sys

logger = logging.getLogger("logger")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


def main():
    a = 0.0
    b = 1.0
    T = 0.05
    k = 1.0
    phi = 0*x
    g1 = 0*t
    g2 = 0*t
    f = x + 0*t

    N = int(raw_input("enter number of x steps: "))
    M = int(raw_input("enter number of t steps: "))

    h = (b - a)/N
    tau = T/M

    xv = [a + i*h for i in xrange(N+1)]
    tv = [a + i*tau for i in xrange(M+1)]

    c = [sp.Symbol("c{}".format(i)) for i in xrange(N + 1)]

    u = array(c*(M + 1), (M + 1, N + 1))

    for i in xrange(N + 1):
        u[0, i] = phi.subs(x, xv[i])

    system = [None]*(N + 1)

    for i in xrange(1, M + 1):
        system[0] = u[i, 0] - g1.subs(t, tv[i])
        system[-1] = (3.*u[i, N] - 4.*u[i, N-1] + u[i, N-2])/(2*h) - g2.subs(t, tv[i])
        # system[-1] = k*(2.*u[i, N-1] + 2*h*g2.subs(t, tv[i]) - 2*u[i, N])/h**2 + \
        # f.subs(t, tv[i]).subs(x, xv[N]) - (u[i, N] - u[i-1, N])/tau

        for j in xrange(1, N):
            system[j] = k*(u[i, j+1] - 2*u[i, j] + u[i, j-1])/h**2 + \
             f.subs(t, tv[i]).subs(x, xv[j]) - (u[i, j] - u[i-1, j])/tau

        result = list(sp.linsolve(system, *c))[0]
        for j in xrange(N + 1):
            u[i, j] = result[j]

    u = np.array(u).reshape((M + 1, N + 1))

    logger.info("\nresult of nonstationary task:")
    for line in u:
        logger.info(", ".join(["%.4f" % i for i in line]))
    logger.info("\n precision ~ {}".format(h**2 + tau))

    for i in xrange(0, M + 1, 2):
        pyplot.plot(xv, u[i, :], label="t = {}".format(i*tau))

    pyplot.legend()
    pyplot.show()


if __name__ == "__main__":
    main()
