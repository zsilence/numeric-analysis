from sympy.tensor.array import MutableDenseNDimArray as array

from matplotlib import pyplot
from sympy.abc import x, t

import sympy as sp
import numpy as np

import math
import logging
import sys

logger = logging.getLogger("logger")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


def main():
    a = 1.0
    b = 2.0
    T = 0.05
    k = x**3
    g1 = 3 + 0*t
    g2 = 0 + 0*t
    phi = (g2 - g1)*(x - a)/(b - a) + g1
    f = 10*x**(1./4) + 0*t

    dk = k.diff(x)

    N = int(raw_input("enter number of x steps: "))
    M = int(raw_input("enter number of t steps: "))

    h = (b - a)/N
    tau = T/M

    xv = [a + i*h for i in xrange(N+1)]
    tv = [a + i*tau for i in xrange(M+1)]

    c = [sp.Symbol("c{}".format(i)) for i in xrange(N + 1)]

    u1 = array(c)

    system = [None]*(N + 1)
    system[0] = c[0] - phi.subs(x, xv[0])
    system[-1] = c[-1] - phi.subs(x, xv[-1])

    for j in xrange(1, N):
        system[j] = tau*k.subs(x, xv[j])*(u1[j+1] - 2*u1[j] + u1[j-1])/h**2 + \
        tau*dk.subs(x, xv[j])*(u1[j+1] - u1[j-1])/(2*h) + tau*f.subs(x, xv[j])

    u1 = list(sp.linsolve(system, *c))[0]

    f = f*(1 - sp.exp(-t))

    u = array(c*(M + 1), (M + 1, N + 1))

    for i in xrange(N + 1):
        u[0, i] = phi.subs(x, xv[i])

    for i in xrange(1, M + 1):
        system[0] = c[0] - g1.subs(t, tv[i])
        system[-1] = c[-1] - g2.subs(t, tv[i])

        for j in xrange(1, N):
            system[j] = k.subs(x, xv[j])*(u[i, j+1] - 2*u[i, j] + u[i, j-1])/h**2 + \
            dk.subs(x, xv[j])*(u[i, j+1] - u[i, j-1])/(2*h) + f.subs(t, tv[i]).subs(x, xv[j]) + \
            -(u[i, j] - u[i-1, j])/tau

        result = list(sp.linsolve(system, *c))[0]
        for j in xrange(N + 1):
            u[i, j] = result[j]

    u = np.array(u).reshape((M + 1, N + 1))

    logger.info("\nresult of stationary task:")
    logger.info(", ".join(["%.4f" % i for i in u1]))
    logger.info("\nresult of nonstationary task:")
    for line in u:
        logger.info(", ".join(["%.4f" % i for i in line]))
    logger.info("\n precision ~ {}".format(h**2 + tau))

    # for i in xrange(0, M + 1, 2):
    for i in xrange(0, M + 1):
        pyplot.plot(xv, u[i, :], label="t = {}".format(i*tau))
    pyplot.plot(xv, u1, label="stationary task", linestyle="--")
    pyplot.legend()
    pyplot.show()


if __name__ == "__main__":
    main()
