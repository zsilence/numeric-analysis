from matplotlib import pyplot

import sympy as sp
import math
import logging
import sys

logger = logging.getLogger("logger")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


def collocation(psi, x, n, a):
    logger.info("system by collocation method:\n")
    system = [psi.subs(x, -1.0 + 2.0*(i+1)/(n+1)) for i in xrange(n)]
    for i in xrange(n):
        logger.info("{} = 0".format(system[i]))
    return system

def iols(psi, x, n, a):
    logger.info("system by integral least squares method:\n")
    integral = sp.integrate(psi**2, (x, -1, 1))
    system = [integral.diff(a[i]) for i in xrange(n)]
    for i in xrange(n):
        logger.info("{} = 0".format(system[i]))
    return system

def dols(psi, x, n, a):
    logger.info("system by discrete least squares method:\n")
    expr = psi.subs(x, -1 + 2*x/n)**2
    s = sp.Sum(expr, (x, 0, n)).doit()
    system = [s.diff(a[i]) for i in xrange(n)]
    for i in xrange(n):
        logger.info("{} = 0".format(system[i]))
    return system

def galerkin(psi, x, n, a):
    logger.info("system by galerkin method:\n")
    system = [sp.integrate(psi*(x**i - x**(i+2)), (x, -1, 1)) for i in xrange(n)]
    for i in xrange(n):
        logger.info("{} = 0".format(system[i]))
    return system

def solve(system, y, n, a):
    result = list(sp.linsolve(system, *a))[0]
    coeffs = [(a[i], result[i]) for i in xrange(n)]
    solution = y.subs(coeffs)
    logger.info("\n{}".format(coeffs))
    logger.info("\ny = {}".format(solution))
    return solution

def precision(solution, x, n, psi):
    a = [sp.Symbol("a{}".format(i+1)) for i in xrange(n*2)]
    y = 0*x
    for i in xrange(n*2):
        y += a[i] * (x**i - x**(i+2))
    if psi == 0:
        psi = sp.simplify(y.diff(x, 2) + (1 + x**2)*y + 1)
    else:
        psi = sp.simplify(math.sin(1)*y.diff(x, 2) + (1 + math.cos(1)*x**2)*y + 1)

    logger.setLevel(logging.CRITICAL)
    f = solve(galerkin(psi, x, n*2, a), y, n*2, a)
    logger.setLevel(logging.INFO)

    eps = sp.integrate((f - solution)**2,(x, -1, 1))/2.0
    logger.info("\nprecision: {}\n".format(eps))

def main():
    n = int(raw_input("enter the number of basic functions: "))
    x = sp.Symbol("x")
    a = [sp.Symbol("a{}".format(i+1)) for i in xrange(n)]
    y = 0*x
    for i in xrange(n):
        y += a[i] * (x**i - x**(i+2))
    psi = [
        sp.simplify(y.diff(x, 2) + (1 + x**2)*y + 1),
        sp.simplify(math.sin(1)*y.diff(x, 2) + (1 + math.cos(1)*x**2)*y + 1)
    ]
    methods = [collocation, iols, dols, galerkin]

    xv = [-1. + i*2./(n*2) for i in xrange(n*2 + 1)]

    for method in methods:
        solution = solve(method(psi[0], x, n, a), y, n, a)
        # precision(solution, x, n, 0)
        pyplot.plot(xv, [solution.subs(x, v) for v in xv], label="1.{}".format(methods.index(method)))

        solution = solve(method(psi[1], x, n, a), y, n, a)
        # precision(solution, x, n, 1)
        pyplot.plot(xv, [solution.subs(x, v) for v in xv], label="2.{}".format(methods.index(method)))


    pyplot.legend()
    pyplot.show()


if __name__ == "__main__":
    main()
