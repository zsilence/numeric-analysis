import sympy as sp
import matplotlib.pyplot as plot
import math
import logging
import sys
import numpy as np

logger = logging.getLogger("logger")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


def diff(yk1, yk, yk_1, x, h, task):
    return (yk1 - 2*yk + yk_1)/(h**2) - yk - 2*x

def calculate(n, h, task, a, b, A, B):
    x = [a + h*i for i in xrange(n + 1)]
    y = [0.0] * (n + 1)
    sy = [sp.Symbol("y{}".format(i)) for i in xrange(n + 1)]

    if task == 0:
        system = [
            sy[0] - 0.5*(-sy[2] + 4*sy[1] - 3*sy[0])/(2*h) - A,
            sy[n] - B
        ]
    else:
        system = [sy[0] - A, sy[n] - B]

    for i in xrange(n - 1):
        system.append(diff(sy[i + 2], sy[i + 1], sy[i], x[i + 1], h, task))


    result = list(sp.linsolve(system, *sy))[0]
    for i in xrange(n + 1):
        y[i] = result[i]

    for i in xrange(n + 1):
        logger.info("{} = 0".format(system[i]))
    logger.info("\n{}\n".format([(x[i], y[i]) for i in xrange(n + 1)]))

    return x, y


n = 23
a, b = 0., 1.
A, B = 0., -1.
h = (b - a)/n
task = 1
x_val, y = calculate(n, h, task, a, b, A, B)


def f(x):
    return np.sinh(x) / np.sinh(1) - 2 * x

t1 = np.arange(0.0, 1.0, 0.002)

plot.plot(t1, f(t1), "k",x_val, y, "bo")

plot.show()
